package com.bs.businesstier.entity;

public class Balance {
	@Override
	public String toString() {
		return "Balance [email=" + email + ", curr_balance=" + curr_balance + ", category=" + category + "]";
	}
	private String email = "";
	private int curr_balance = 0;
	private String category = "";
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCurr_balance() {
		return curr_balance;
	}
	public void setCurr_balance(int curr_balance) {
		this.curr_balance = curr_balance;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

}

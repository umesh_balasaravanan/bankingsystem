package com.bs.businesstier.entity;

public class Account {

	private String emailaddress = "";
	private String accountnumber = "";
	private int swift_code = 0;
	private int routing_num = 0;
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public int getSwift_code() {
		return swift_code;
	}
	public void setSwift_code(int swift_code) {
		this.swift_code = swift_code;
	}
	public int getRouting_num() {
		return routing_num;
	}
	public void setRouting_num(int routing_num) {
		this.routing_num = routing_num;
	}
	
}
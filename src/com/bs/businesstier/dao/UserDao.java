package com.bs.businesstier.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bs.businesstier.entity.User;
import com.bs.businesstier.helper.BsDataConnection;
import com.bs.businesstier.helper.BsDbQuery;


public class UserDao {
	private static Logger log = Logger.getLogger(UserDao.class);
	private Connection connection = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private BsDbQuery query;
	
	public UserDao() {
		ApplicationContext context =  new ClassPathXmlApplicationContext("/applicationConfig.xml");
		query = (BsDbQuery)context.getBean("sqlbean");
		
	}
	
	public User searchUser2(String username, String password) throws ClassNotFoundException, SQLException {
	
		connection = BsDataConnection.createConnection();
		System.out.println(query.getSearchQuery());
		ps = connection.prepareStatement(query.getSearchQuery());
		ps.setString(1, username);
		ps.setString(2, password);
		System.out.println(ps.toString());
		rs = ps.executeQuery();
		//ArrayList<User> userList = new ArrayList<>();
		User user = new User();
		System.out.println("before while block");
		while(rs.next()) {
			user.setEmailId(rs.getString(1));
			//System.out.println(user.getEmailId());
			user.setPassword(rs.getString(2));
			user.setFname(rs.getString(3));
			user.setLname(rs.getString(4));
			user.setPhoneNumber(rs.getString(5));
			user.setAddress(rs.getString(6));
			//userList.add(user);
		}
		return user;
		
	}
}

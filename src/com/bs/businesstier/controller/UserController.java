package com.bs.businesstier.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bs.businesstier.entity.Account;
import com.bs.businesstier.entity.Balance;
import com.bs.businesstier.entity.User;
import com.bs.businesstier.helper.BsDataConnection;
import com.bs.businesstier.helper.BsDbQuery;
import com.bs.businesstier.service.UserServiceImpl;



@Controller
public class UserController {
	private static Logger log = Logger.getLogger(UserController.class);
	private Connection connection = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private BsDbQuery query;
	private Statement stmt;
	
// uncomment code from line # 39 to 80 to perform attacks on login page(by Umesh & Mary)
	/*@RequestMapping("/searchUser.htm")
	public ModelAndView searchUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException{
	ModelAndView mv = new ModelAndView();
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	connection = BsDataConnection.createConnection();
	String q = String.format("select * from user where emailId = %s and password = %s", username,password);
	System.out.println(q);
	stmt=connection.createStatement(); 
	rs = stmt.executeQuery(q);
	System.out.println(rs.toString());
	User user = new User();
	
	while(rs.next()) {
	user.setEmailId(rs.getString(1));
	user.setPassword(rs.getString(2));
	user.setFname(rs.getString(3));
	user.setLname(rs.getString(4));
	user.setPhoneNumber(rs.getString(5));
	user.setAddress(rs.getString(6));
	}
	if(user.getEmailId()!= null) {
	if(username.contains(user.getEmailId())) {
	System.out.println("inside if clause");
	mv.addObject(user);
	mv.setViewName("/homepage.jsp");
	return mv;
	}else {
	System.out.println("inside else clause");
	mv.addObject("ERROR", "Invalid Username / Password.");
	mv.setViewName("/BankLogin.jsp");
	return mv;
	}
	}
	else {
	System.out.println("inside else clause");
	mv.addObject("ERROR", "Invalid Username / Password.");
	mv.setViewName("/BankLogin.jsp");
	return mv;
	}
	}*/
	
	// prevention for attacks on login page. To perform attack, comment code from line # 83 to 122 (by Umesh & Mary)
	@RequestMapping("/searchUser.htm")
	public ModelAndView searchUser(HttpServletRequest request,HttpServletResponse response) throws SQLException, ClassNotFoundException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		System.out.println("username "+ username);
		ModelAndView mv = new ModelAndView();
		if(username.contains("\'") || password.contains("\'") || username.contains("--")
				|| password.contains("--") || username.contains("update") || username.contains("delete")||
				username.contains("drop") || username.contains("select") || username.contains("insert") ||
				password.contains("update") || password.contains("delete")|| password.contains("drop") || 
				password.contains("select") || password.contains("insert") || username.contains("sleep") || 
				password.contains("sleep")){
			mv.addObject("ERROR", "SQL INJECTION DETECTED");
			mv.setViewName("/BankLogin.jsp");
			return mv;
		}else {
		User user = new User();
		UserServiceImpl userviceimpl = new UserServiceImpl();
		System.out.println("calling searchUser1 method in UserServiceImpl class");
		user = userviceimpl.searchUser1(username, password);
		System.out.println("user name " + username);
		System.out.println("email id " + user.getEmailId());
		boolean condition = username.equals(user.getEmailId());
		System.out.println("condition value : " + condition);
		System.out.println(" " + user.getEmailId());
		if(username.equals(user.getEmailId())) {
			System.out.println("inside if clause");
			mv.addObject(user);
			mv.setViewName("/homepage.jsp");
			return mv;
		}
		else {
			System.out.println("inside else clause");
			mv.addObject("ERROR", "Invalid Username / Password.");
			mv.setViewName("/BankLogin.jsp");
			return mv;
	}
	}
	}
	
	//Uncomment these to check attacks in registration.jsp, for ON DUPLICATE KEY UPDATE ATTACK BY MARY//
//	@RequestMapping("/insertUser.htm")
//	public ModelAndView insertUser(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
//		ModelAndView mv = new ModelAndView();
//		String emailId = request.getParameter("emailId");
//		String password = request.getParameter("password");
//		String fname = request.getParameter("fname");
//		String lname = request.getParameter("lname");
//		String phoneNumber = request.getParameter("phoneNumber");
//		String address = request.getParameter("address");
//		connection = BsDataConnection.createConnection();
//		String q = String.format("insert into user(emailId, password, fname, lname, phoneNumber, address) values(%s, %s, %s, %s, %s, %s)", emailId, password, fname, lname, phoneNumber, address);
//		System.out.println(q);
//		stmt=connection.createStatement(); 
//		//ps = connection.prepareStatement(q);
//		int insert = stmt.executeUpdate(q);
//		System.out.println("inserted successfully");
//			mv.addObject("MSG", "New User Inserted Succesfully");
//			mv.setViewName("/registration.jsp");
//		
//		
//		return mv;
//	}
	
	//Prevention for attacks in registration.jsp//
	@RequestMapping("/insertUser.htm")
	public ModelAndView insertUser(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
		ModelAndView mv = new ModelAndView();
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String phoneNumber = request.getParameter("phoneNumber");
		String address = request.getParameter("address");
		if(emailId.contains("\'")|| password.contains("\'") || fname.contains("\'") 
				|| lname.contains("\'") || phoneNumber.contains("\'") || address.contains("\'") ||
				address.contains("--")|| address.contains("update") || address.contains("delete")||
				address.contains("drop") || address.contains("select") || address.contains("insert")) {
			mv.addObject("ERROR","SQL INJECTION DETECTED");
			
		}else {
		connection = BsDataConnection.createConnection();
		ApplicationContext context =  new ClassPathXmlApplicationContext("/applicationConfig.xml");
		query = (BsDbQuery)context.getBean("sqlbean");
		connection = BsDataConnection.createConnection();
		ps = connection.prepareStatement(query.getInsertUserQuery());
		ps.setString(1, emailId);
		ps.setString(2, password);
		ps.setString(3, fname);
		ps.setString(4, lname);
		ps.setString(5,phoneNumber);
		ps.setString(6, address);
		System.out.println(ps.toString());
		int insert = ps.executeUpdate();
		if(insert == 1) {
			System.out.println("inserted successfully");
			mv.addObject("MSG", "New User Inserted Succesfully");
			//mv.setViewName("/registration.jsp");
		}else {
			mv.addObject("MSG","insertion unsuccessful");
			//mv.setViewName("/registration.jsp");
		}
		}
		mv.setViewName("/registration.jsp");
		return mv;
	}

	// uncomment code from line #192 to 220 to perform attacks on accountdetails page. (by Umesh)
	/*@RequestMapping("/accountInfo.htm")
	public ModelAndView accountInfo(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
	ModelAndView mv = new ModelAndView();
	String email = request.getParameter("email");
	connection = BsDataConnection.createConnection();
	String q = null;
	if(email.equals("admin")) {
		q = "select * from account";
	}else {
	q = "select * from account where emailaddress = '" + email + "'";
	}
	System.out.println(q);
	stmt=connection.createStatement(); 
	rs = stmt.executeQuery(q);
	ArrayList<Object[]> visitorsList = new ArrayList<Object[]>();
	while(rs.next()) {
		Object[] visitorObject = new Object[4];
		visitorObject[0] = rs.getString(1);
		visitorObject[1] = rs.getString(2);
		visitorObject[2] = rs.getString(3);
		visitorObject[3] = rs.getString(4);
		visitorsList.add(visitorObject);
		
	}
	rs.close();
	mv.addObject("visitors", visitorsList);
	mv.setViewName("/AccountDetails.jsp");
	return mv;
	} */
	
	//prevention for attacks that can be performed on AccountDetails page. comment code from line # 223 to 256 while performing attack on account details page, (by Umesh)
	@RequestMapping("/accountInfo.htm")
	public ModelAndView accountInfo(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
	ModelAndView mv = new ModelAndView();
	ApplicationContext context =  new ClassPathXmlApplicationContext("/applicationConfig.xml");
	query = (BsDbQuery)context.getBean("sqlbean");
	connection = BsDataConnection.createConnection();
	String email = request.getParameter("email");
	if(email.contains("select") || email.contains("update") || email.contains("drop") || email.contains("truncate") || email.contains("union")){
	mv.addObject("ERROR","SQL INJECTION DETECTED");
	}else {
	if(email.equals("admin")) {
	ps = connection.prepareStatement(query.getAdminaccountInfoQuery());
	}else {
	ps = connection.prepareStatement(query.getAccountInfoQuery());
	ps.setString(1, email);
	System.out.println(ps.toString());
	}
	rs = ps.executeQuery();
	ArrayList<Object[]> visitorsList = new ArrayList<Object[]>();
	while(rs.next()) {
	Object[] visitorObject = new Object[4];
	visitorObject[0] = rs.getString(1);
	visitorObject[1] = rs.getString(2);
	visitorObject[2] = rs.getString(3);
	visitorObject[3] = rs.getString(4);
	visitorsList.add(visitorObject);

	}
	rs.close();
	mv.addObject("visitors", visitorsList);
	}
	mv.setViewName("/AccountDetails.jsp");
	return mv;
	}
	
	//uncomment code from line# 259 to 286 to perform attacks on balance details page.(by Umesh)
	/*@RequestMapping("/balanceInfo.htm")
	public ModelAndView balanceInfo(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
	ModelAndView mv = new ModelAndView();
	String email = request.getParameter("email");
	connection = BsDataConnection.createConnection();
	String q = null;
	if(email.contentEquals("admin")) {
		q = "select * from balance";
	}else {
		q = "select * from balance where email = '" + email + "'";
	}
	System.out.println(q);
	stmt=connection.createStatement(); 
	rs = stmt.executeQuery(q);
	ArrayList<Object[]> balancesList = new ArrayList<Object[]>();
	while(rs.next()) {
		Object[] balanceObject = new Object[3];
		balanceObject[0] = rs.getString(1);
		balanceObject[1] = rs.getString(2);
		balanceObject[2] = rs.getString(3);
		balancesList.add(balanceObject);
		
	}
	rs.close();
	mv.addObject("balances", balancesList);
	mv.setViewName("/Balance.jsp");
	return mv;
	}*/
	
	//Prevention on balance details page. comment code from line# 289 to 322 perform attack on balance details page. (by Umesh)
	@RequestMapping("/balanceInfo.htm")
	public ModelAndView balanceInfo(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException {
	ModelAndView mv = new ModelAndView();
	String email = request.getParameter("email");
	connection = BsDataConnection.createConnection();
	ApplicationContext context =  new ClassPathXmlApplicationContext("/applicationConfig.xml");
	query = (BsDbQuery)context.getBean("sqlbean");
	if(email.contains("select") || email.contains("update") || email.contains("drop") || email.contains("truncate") || email.contains("union")){
	mv.addObject("ERROR","SQL INJECTION DETECTED");
	}else {
	if(email.equals("admin")) {
	ps = connection.prepareStatement(query.getAdminbalanceInfoQuery());
	System.out.println(ps.toString());
	}else {
	ps = connection.prepareStatement(query.getBalanceInfoQuery());
	ps.setString(1, email);
	System.out.println(ps.toString());
	}
	rs = ps.executeQuery();
	ArrayList<Object[]> balancesList = new ArrayList<Object[]>();
	while(rs.next()) {
	Object[] balanceObject = new Object[3];
	balanceObject[0] = rs.getString(1);
	balanceObject[1] = rs.getString(2);
	balanceObject[2] = rs.getString(3);
	balancesList.add(balanceObject);

	}
	rs.close();
	mv.addObject("balances", balancesList);
	}
	mv.setViewName("/Balance.jsp");
	return mv;
	}
}
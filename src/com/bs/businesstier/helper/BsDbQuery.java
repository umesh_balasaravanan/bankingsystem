package com.bs.businesstier.helper;

public class BsDbQuery {
	private String searchQuery = "";
	private String insertUserQuery = "";
	private String accountInfoQuery = "";
	private String adminaccountInfoQuery ="";
	private String balanceInfoQuery = "";
	private String adminbalanceInfoQuery = "";

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getInsertUserQuery() {
		return insertUserQuery;
	}

	public void setInsertUserQuery(String insertUserQuery) {
		this.insertUserQuery = insertUserQuery;
	}

	public String getAccountInfoQuery() {
		return accountInfoQuery;
	}

	public void setAccountInfoQuery(String accountInfoQuery) {
		this.accountInfoQuery = accountInfoQuery;
	}

	public String getAdminaccountInfoQuery() {
		return adminaccountInfoQuery;
	}

	public void setAdminaccountInfoQuery(String adminaccountInfoQuery) {
		this.adminaccountInfoQuery = adminaccountInfoQuery;
	}

	public String getBalanceInfoQuery() {
		return balanceInfoQuery;
	}

	public void setBalanceInfoQuery(String balanceInfoQuery) {
		this.balanceInfoQuery = balanceInfoQuery;
	}

	public String getAdminbalanceInfoQuery() {
		return adminbalanceInfoQuery;
	}

	public void setAdminbalanceInfoQuery(String adminbalanceInfoQuery) {
		this.adminbalanceInfoQuery = adminbalanceInfoQuery;
	}
	
}

package com.bs.exceptions;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class BsGenericException extends Exception {

	private static Logger log = Logger.getLogger(BsGenericException.class);

	public BsGenericException(String message, Throwable object) {
		super(message, object);
		log.info("Exception Message is :" + message);
	}
}

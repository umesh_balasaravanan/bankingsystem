## Team Project  ##

University of Dayton 

Department of Computer Science

CPS 499-02/592-02, Software/Language based Security 

Instructor: Dr. Phu Phung

# Advanced SQL injection attacks and defense #

## Team 5 - Members: ##

1. Umesh Balasaravanan <balasaravananu1@udayton.edu>
2. Mary Shalin Stanley <stanleypremkumarm1@udayton.edu>

# Project Management #

Trello Board Details: 

1. Team name : SS-LBS-F20-GROUP5

2. Board Name: lbs final project 

3. Lists : 

	a. Brainstorm - to discuss about our ideas and references/resources collected related to the project
	b. ToDo - Once the discussion is unanimously accepted, tasks are listed and assigned along with the description and deadline 
	c. Doing - After the team member picks a task from the TODO list, they will move it to this secction 'Doing' to show the work progress 
	d. Done - After completing the task, the member marks it as 'DONE' by moving it to this list

Board Link - https://trello.com/b/tc6n3gcD/lbs-final-project

Team Repository Information: 

1. Workspace name: ss-lbs-f20-group5 
2. Repository name: Banking System 
3. Project name:lbs_project

Bitbucket Link - https://bitbucket.org/umesh_balasaravanan/bankingsystem/src/master/


# Introduction #

SQL Injection is a code injection technique, used to attack data driven applications. We have created a prototype
of a banking system to show how an SQL Injection based attacks can happen in real time and prevention techniques
to overcome those attacks. This application is built using JSP as a frontend, Spring as a middleware and Mysql as
a backend. This application has a login module that allows customers and admin to enter their respective username
password to access/retrieve sensitive information such as account info, balance info etc., and a registration module
that allows customers to enter their personal details to create an online account for this banking system.

The aim of this project is to find sqli based vulnerabilities in this application, perform attacks and implement sqli
prevention techniques.

# Implementation #
1. Install eclipse, Apache tomcat server in your system. 
apache tomcat server can be downloaded from this url : https://tomcat.apache.org/download-70.cgi 
2. In quick access, search for Add a git repository and enter git clone https://umesh_balasaravanan@bitbucket.org/umesh_balasaravanan/bankingsystem.git 
3. After cloning it successful, open git repositories perspective.
4. Right click the git folder and click import projects to get the project imported to your local machine. 
5. In project explorer, right click the project and click build path -> configure build path
  Open targeted runtime and select the apache server that you installed in your system previously. 
6. Right click the BankingSystem project again and click Run as -> Run on server to run the application on server/web browser successfully. 
7. Before running the project on your local machine, make sure that mysql server is installed and configured in your local machine.
8. Run the sql commands provided in the BankingSystem.sql file one by one/whole in the mysql server and make sure that all the required tables and tuples listed in the BankingSystem.sql file are present in the local machine.
9. In BankingSystem project,
open Java Resources -> src -> com.bs.businesstier.helper -> BsDataConnection java file and set the mysql server connection, username and password in sync with your local machine setup.

Now, you can run the project BankingSystem successfully on your local machine. 
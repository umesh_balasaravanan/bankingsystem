create database bankingsystem;

use bankingsystem;

create table user(emailId varchar(40) not null, password varchar(40) not null,
 fname varchar(20) not null, lname varchar(20) not null, phoneNumber varchar(15) not null,
 address varchar(40) not null,primary key(emailId));
 
 create table account(emailaddress varchar(40) not null, accountnumber varchar(40) not null, swift_code integer not null, 
 routing_num integer not null, foreign key(emailaddress) references user(emailId));
 
 create table balance(email varchar(40) not null, curr_balance integer not null, category varchar(40) not null, 
 foreign key(email) references user(emailId));
 
insert into user(emailId, password, fname, lname, phoneNumber, address) 
values('us14696@gmail.com', 'password', 'umesh', 'balasaravanan', '+19378135838', 'Dayton');
insert into user(emailId, password, fname, lname, phoneNumber, address) 
values('maryshalin@gmail.com', 'password', 'mary', 'shalin', '+19876543212', 'Dayton');

insert into user(emailId, password, fname, lname, phoneNumber, address) 
values('admin', 'admin', 'Admin', 'Admin', '1234567', 'Dayton');

insert into account(emailaddress, accountnumber, swift_code, routing_num)
values('us14696@gmail.com','aaaaaa1', 10199, '000100');

insert into account(emailaddress, accountnumber, swift_code, routing_num)
values('maryshalin@gmail.com','aaaaaa2', 20192, '00200');

insert into balance(email, curr_balance, category)
values('us14696@gmail.com', '1000', 'checking');

insert into balance(email, curr_balance, category)
values('maryshalin@gmail.com', '2500', 'savings');










 
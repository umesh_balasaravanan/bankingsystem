<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome to Banking System</title>
</head>
<style>
.msg {
	color: green;
	font-size: 13px;
	font-weight: bold;
}
.error {
	color: red;
	font-size: 13px;
	font-weight: bold;
}
</style>
<script type = "text/javascript"> 
function validateForm(){
	var uname = document.forms["contactInforForm"]["emailId"].value;
	var password = document.forms["contactInforForm"]["password"].value;
	var fname = document.forms["contactInforForm"]["fname"].value;
	var lname = document.forms["contactInforForm"]["lname"].value;
	var pnum = document.forms["contactInforForm"]["phoneNumber"].value;
	var address = document.forms["contactInforForm"]["address"].value;
	if(uname == "" || uname == null){
		alert("please provide the user name");
		return false;
	}
	if(password == "" || password == null){
		alert("please provide the password");
		return false;
		}
	if(fname == "" || fname == null){
		alert("please provide the firstname");
		return false;
		}
	if(lname == "" || lname == null){
		alert("please provide the lastname");
		return false;
		}
	if(pnum == "" || pnum == null){
		alert("please provide the phone number");
		return false;
		}
	if(address == "" || address == null){
		alert("please provide the address");
		return false;
		}
	
	}
	
	</script>
<body>
<form action="insertUser.htm" method = "post" name = "contactInforForm" onsubmit = "return validateForm()">
<table width = " 100%" height="100%" align = "center" style="background-color: white">
<tr>
<td><h1 align = "center">Banking System</h1>
<table align = "center">
<tr>
<td>Email   : </td>
<td><input type = "text" name = "emailId"></td></tr>
<tr>
<td>Password  : </td>
<td><input type = "text" name = "password"></td></tr>
<tr>
<td>First Name   : </td>
<td><input type = "text" name = "fname"></td></tr>
<tr>
<td>Last Name    : </td>
<td><input type = "text" name = "lname"></td></tr>
<tr>
<td>Phone Number : </td>
<td><input type = "text" name = "phoneNumber"></td></tr>
<tr>
<td>Address      : </td>
<td><input type = "text" name = "address"></td></tr>
<tr>
<td><input type = "submit" value = "submit"></td>
<td><input type = "reset" value = "cancel"></td>
<tr>
<td colspan="2" align="center"><span class="msg">${MSG}</span></td>
</tr>
<tr>
<td colspan="2" align="center"><span class="error">${ERROR}</span></td>
</tr>
</table></td>
</tr>
</table>
</form>
</body>
</html>